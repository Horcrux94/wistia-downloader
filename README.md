# Setup

Have nodejs and npm installed.

Execute the following in the folder:

`npm install`

`tsc`

`node download_thing.js`

If you did things right you'll see some links ending in `.bin`. Download these through a browser and change their file extensions to .mp4.

# Usage

Open `download_thing.ts`.

Right click the target videos and click "Copy link and thumbnail". Paste this info in the `linkAndThumbnails` array. Use backticks (`) around them, and commas to seperate them.

Execute in the folder:

`tsc; node download_thing.js`

If you did things right you'll see some links ending in `.bin`. Download these through a browser and change their file extensions to .mp4.
