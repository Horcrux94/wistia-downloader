var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import fetch from "node-fetch";
let linkAndThumbnails = [`<p><a href="https://docusign.wistia.com/medias/0g8dd3erj7?wvideo=0g8dd3erj7"><img src="https://embed-fastly.wistia.com/deliveries/fe3d7544ecd11412dd3e0bbd144417d1d49b8bee.jpg?image_play_button_size=2x&amp;image_crop_resized=960x540&amp;image_play_button=1&amp;image_play_button_color=005cb9e0" style="width: 400px; height: 225px;" width="400" height="225"></a></p><p><a href="https://docusign.wistia.com/medias/0g8dd3erj7?wvideo=0g8dd3erj7">CLM_JoshPendergrass - DocuSign, Inc.</a></p>`,
    `<p><a href="https://docusign.wistia.com/medias/osv6ljw6id?wvideo=osv6ljw6id"><img src="https://embed-fastly.wistia.com/deliveries/ce39f2e0487173837c3dddcc3d74b34baa4d7bb6.jpg?image_play_button_size=2x&amp;image_crop_resized=960x540&amp;image_play_button=1&amp;image_play_button_color=005cb9e0" style="width: 400px; height: 225px;" width="400" height="225"></a></p><p><a href="https://docusign.wistia.com/medias/osv6ljw6id?wvideo=osv6ljw6id">Salesforce - DocuSign, Inc.</a></p>`,
    `<p><a href="https://docusign.wistia.com/medias/3pzh251irj?wvideo=3pzh251irj"><img src="https://embedwistia-a.akamaihd.net/deliveries/d396ce520a8e45577054ffd19470590247c97803.jpg?image_play_button_size=2x&amp;image_crop_resized=960x540&amp;image_play_button=1&amp;image_play_button_color=005cb9e0" style="width: 400px; height: 225px;" width="400" height="225"></a></p><p><a href="https://docusign.wistia.com/medias/3pzh251irj?wvideo=3pzh251irj">Expression Builder - DocuSign, Inc.</a></p>`,
    `<p><a href="https://docusign.wistia.com/medias/lh0qja0ts6?wvideo=lh0qja0ts6"><img src="https://embedwistia-a.akamaihd.net/deliveries/490598ce4844d93ca58f8573ea271e98c9df6124.jpg?image_play_button_size=2x&amp;image_crop_resized=960x540&amp;image_play_button=1&amp;image_play_button_color=005cb9e0" style="width: 400px; height: 225px;" width="400" height="225"></a></p><p><a href="https://docusign.wistia.com/medias/lh0qja0ts6?wvideo=lh0qja0ts6">SDK_Josh Pendergrass 3 - DocuSign, Inc.</a></p>`];
linkAndThumbnails.forEach((linkAndThumbnail) => __awaiter(void 0, void 0, void 0, function* () {
    const link = linkAndThumbnail.split("\"")[1];
    const id = link.split("/").pop();
    const url1 = `https://fast.wistia.net/embed/iframe/${id}?videoFoam=true`;
    const restext = yield (yield fetch(url1)).text();
    const json = restext.slice(restext.indexOf("W.iframeInit") + "W.iframeInit(".length, restext.length - 80);
    const dllink = JSON.parse(json).assets.find(asset => asset.type === "original").url;
    console.log(dllink);
}));
